-- 1) Выберите заказчиков из Германии, Франции и Мадрида, выведите их название, страну и адрес.

SELECT contactname, country, address FROM Customers where country = 'Germany' or country = 'France'or city = 'Madrid';

-- 2) Выберите топ 3 страны по количеству заказчиков, выведите их названия и количество записей.

SELECT country, count(country) as c FROM [Customers] group by country order by c desc limit 3;

-- 3) Выберите перевозчика, который отправил 10-й по времени заказ, выведите его название, и дату отправления.

SELECT orderdate, shippername FROM orders as o
join shippers as s on o.shipperid = s.shipperid
order by orderdate
limit 1 offset 9;

-- 4) Выберите самый дорогой заказ, выведите список товаров с их ценами.

select productname, price from orderdetails as od
join (
    SELECT orderid, sum(price*quantity) as orderPrice FROM [OrderDetails] as od
    join products as p on p.productid = od.productid
    group by orderid
    order by orderPrice desc
    limit 1
) as r on r.orderid = od.orderid
join products as p on p.productid = od.productid


-- 5) Какой товар больше всего заказывали по количеству единиц товара, выведите его название и количество единиц в каждом из заказов.
SELECT productname, sum(quantity) as totalQuantity FROM OrderDetails as o
join products as p on o.productid = p.productid
group by p.productid
order by totalQuantity desc limit 1

-- 6) Выведите топ 5 поставщиков по количеству заказов, выведите их названия, страну, контактное лицо и телефон.

SELECT suppliername, contactname, country, phone, count(o.orderid) as orderAmount FROM [Orders] as o
join orderdetails as od on o.orderid = od.orderid
join products as p on p.productid = od.productid
join suppliers as s on s.supplierid = p.supplierid
group by s.supplierid
order by orderAmount desc
limit 5

-- 7) Какую категорию товаров заказывали больше всего по стоимости в Бразилии, выведите страну, название категории и сумму.

select p.categoryid, sum(quantity*price) as totalPrice, country from orderdetails as od
join orders as o on od.orderid = o.orderid
join customers as c on c.customerid = o.customerid
join products as p on od.productid = p.productid
join categories as cat on cat.categoryid = p.categoryid
where country = 'Brazil'
group by cat.categoryid
limit 1

-- 8) Какая разница в стоимости между самым дорогим и самым дешевым заказом из США.

select max(orderPrice) - min(orderPrice) from (
    SELECT o.orderid, customername, (quantity*price) as orderPrice FROM [OrderDetails] as od
    join orders as o on o.orderid = od.orderid
    join customers as c on c.customerid = o.customerid
    join products as p on p.productid = od.productid
    where c.country = 'USA'
    group by o.orderid
)

-- 9) Выведите количество заказов у каждого их трех самых молодых сотрудников, а также имя и фамилию во второй колонке.

SELECT count(o.orderid) as orderAmount, lastname FROM [Employees] as e
join orders as o on o.employeeid = e. employeeid
group by o.employeeid
order by birthdate desc
limit 3

-- 10) Сколько банок крабового мяса всего было заказано.

SELECT sum(quantity) as crabMeatQuantity FROM [OrderDetails] as o
join products as p on o.productid = p.productid
where p.productname = 'Boston Crab Meat'

